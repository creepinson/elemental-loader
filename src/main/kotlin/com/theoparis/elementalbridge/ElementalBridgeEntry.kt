package com.theoparis.elementalbridge

import com.theoparis.elementalbridge.mappings.MappingsLoader
import net.bytebuddy.agent.builder.AgentBuilder
import java.io.File
import java.lang.instrument.Instrumentation
import java.nio.file.Files
import java.util.logging.Logger
import kotlin.concurrent.thread

/**
 * The entrypoint for the java agent.
 */
object ElementalBridgeEntry {
    private val log = Logger.getLogger("ElementalModder")

    @JvmStatic
    fun premain(arg: String?, instrumentation: Instrumentation) {
        thread(start = true) {
            log.info { "Loading Elemental Bridge..." }
            log.info { "Loading minecraft mappings..." }
            MappingsLoader.loadMappings()

            log.info { "Loading byte buddy..." }
            AgentBuilder.Default()
                .with(AgentBuilder.InitializationStrategy.NoOp.INSTANCE)
                .with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
                .with(AgentBuilder.TypeStrategy.Default.REDEFINE)
                .installOn(instrumentation)
            // TODO: maybe hook into the minecraft code for bootstrapping?

            while (!CoreLoader.isBootstrapped())
                Thread.sleep(100)

            log.info { "Done initializing." }

            val engine = ModEngine().init()

            // TODO: mod loader (mods folder)
            val modsFolder = File("mods")
            if (!modsFolder.exists())
                modsFolder.mkdirs()

            val modFileList = Files.walk(modsFolder.toPath().toAbsolutePath())
                .filter { item -> Files.isRegularFile(item) }
                .filter { item -> item.toString().endsWith(".mjs") or item.toString().endsWith(".js") }

            for (modFile in modFileList) {
                engine.eval(modFile.toFile())
            }
        }
    }
}
