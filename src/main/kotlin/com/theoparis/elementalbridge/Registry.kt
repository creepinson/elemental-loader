package com.theoparis.elementalbridge

import com.theoparis.elementalbridge.core.Item
import com.theoparis.elementalbridge.mappings.MappingsLoader
import java.util.logging.Logger

class Registry<T>(val modId: String, val type: String) {
    companion object {
        val logger = Logger.getLogger("ElementalRegistry")
    }

    fun register(obj: T) {
        val lifeCycleClass = Class.forName(MappingsLoader.getClassDotted("com/mojang/serialization/Lifecycle"))
        val lifeCycle =
            lifeCycleClass.getMethod(MappingsLoader.getMethod("stable", "com/mojang/serialization/Lifecycle"))
                .invoke(null)
        val registryKeyClass = Class.forName(MappingsLoader.getClassDotted("net/minecraft/util/registry/RegistryKey"))
        val identifierClass = Class.forName(MappingsLoader.getClassDotted("net/minecraft/util/Identifier"))
        val registryKeyMethod =
            registryKeyClass.getMethod(
                MappingsLoader.getMethod(
                    "of",
                    "net/minecraft/util/registry/RegistryKey"
                ),
                registryKeyClass,
                identifierClass
            )

        if (type == "item" && obj is Item) {
            logger.info("Registering item: ${modId}:${obj.id}")
//            println(Class.forName(MappingsLoader.getClassDotted("net/minecraft/util/registry/Registry")).simpleName)
            val registryClass = Class.forName(MappingsLoader.getClassDotted("net/minecraft/util/registry/Registry"))

            val registryField =
                registryClass.getField(
                    MappingsLoader.getField(
                        "ITEM", "net/minecraft/util/registry/Registry"
                    )
                )
            val registryKeyField = registryClass.getField(
                MappingsLoader.getField(
                    "ITEM_KEY",
                    "net/minecraft/util/registry/Registry"
                )
            )
            val registryValue = registryField.get(null)
            val defaultedRegistryClass =
                Class.forName(MappingsLoader.getClassDotted("net/minecraft/util/registry/MutableRegistry"))
            val registryMethod = defaultedRegistryClass.getMethod(
                MappingsLoader.getMethod(
                    "add",
                    "net/minecraft/util/registry/MutableRegistry",
                ),
                registryKeyClass,
                Object::class.java,
                lifeCycleClass
            )

            val itemClass = Class.forName(MappingsLoader.getClassDotted("net/minecraft/item/Item"))
            val itemSettingsClass = Class.forName(MappingsLoader.getClassDotted("net/minecraft/item/Item\$Settings"))

            val generatedItemInstance = itemClass.getConstructor(itemSettingsClass).newInstance(
                itemSettingsClass.getConstructor().newInstance()
            )

            val registryKey = registryKeyMethod.invoke(
                null,
                registryKeyField.get(null),
                identifierClass.getConstructor(String::class.java)
                    .newInstance("${modId}:${obj.id}")
            )
            registryMethod.invoke(registryValue, registryKey, generatedItemInstance, lifeCycle)
        }
    }
}
