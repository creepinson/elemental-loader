package com.theoparis.elementalbridge

import com.theoparis.elementalbridge.core.Item
import java.io.File
import java.io.FileReader
import java.util.logging.Logger
import javax.script.ScriptContext
import javax.script.ScriptEngine
import javax.script.ScriptEngineManager


class ModEngine {
    lateinit var scriptEngine: ScriptEngine
        private set

    fun init(): ModEngine {
        this.scriptEngine = ScriptEngineManager().getEngineByName("graal.js")

/*
        this.context = Context
            .newBuilder("js")
            .allowHostAccess(HostAccess.ALL)
            .build()
*/
        val bindings = scriptEngine.getBindings(ScriptContext.ENGINE_SCOPE)
        bindings["polyglot.js.allowAllAccess"] = true

        bindings["console"] = Logger.getLogger("ElementalEngine")
        bindings["Registry"] = Registry::class.java
        bindings["Item"] = Item::class.java

        return this
    }

    fun eval(source: String): Any? {
        return scriptEngine.eval(source)
    }


    fun eval(file: File): Any? {
        return scriptEngine.eval(FileReader(file))
    }
}