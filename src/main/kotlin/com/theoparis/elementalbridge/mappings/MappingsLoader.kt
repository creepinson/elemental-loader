package com.theoparis.elementalbridge.mappings

import com.google.gson.GsonBuilder

class MappingsLoader {
    companion object {
        private var mappings = MappingsList(mutableListOf(), mutableListOf(), mutableListOf())

        @Suppress("UNCHECKED_CAST")
        @JvmStatic
        fun loadMappings() {
            val text = this::class.java.classLoader.getResource("mappings.json")?.readText()
            mappings = GsonBuilder().setPrettyPrinting().create().fromJson(
                text,
                MappingsList::class.java
            )
        }

        @JvmStatic
        fun getClass(name: String): String {
//            println(name)
/*            println(
                mappings.classes.firstOrNull {
                    it.originalName == name.replace(".", "/")
                }?.mappedName
            )*/
            val mappingId =
                mappings.classes.firstOrNull {
                    it.originalName == name.replace(".", "/")
                }?.mappedName ?: name
            return mappingId
        }

        @JvmStatic
        fun getClassDotted(name: String): String {
            return getClass(name).replace("/", ".")
        }

        @JvmStatic
        fun getMethod(name: String, className: String): String {
            val classMappingId = getClass(className)

            val methodMappingId = mappings.methods.find {
                it.className.endsWith(classMappingId) && it.originalName.startsWith(name)
            }?.mappedName ?: name

            return methodMappingId
        }

        @JvmStatic
        fun getField(name: String, className: String): String {
            val classMappingId = getClass(className)

            val fieldMappingId = mappings.fields.find {
                it.className.endsWith(classMappingId) && it.originalName.startsWith(name)
            }?.mappedName ?: name

            return fieldMappingId
        }
    }
}
