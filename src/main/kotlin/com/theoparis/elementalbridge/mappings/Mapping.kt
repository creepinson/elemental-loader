package com.theoparis.elementalbridge.mappings

data class FieldMapping(var className: String, var originalName: String, var mappedName: String, var type: String)

data class ClassMapping(
    var originalName: String,
    var mappedName: String,
)

data class MethodMapping(
    var className: String,
    var originalName: String,
    var mappedName: String?,
    var signature: String
)

data class MappingsList(
    var classes: MutableList<ClassMapping>,
    var fields: MutableList<FieldMapping>,
    var methods: MutableList<MethodMapping>
)
