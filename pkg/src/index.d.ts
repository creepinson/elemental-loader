declare class Polyglot {
  static import<T>(name: string): T;
}

declare class Java {
  static type<T>(fullName: string): T;
}
