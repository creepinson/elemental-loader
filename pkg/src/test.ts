import { Mod, IItem } from "./index";

const myMod = new Mod({
  id: "mymod",
});

const itemRegistry = myMod.createRegistry<IItem>("item");

const item = myMod.createItem({
  id: "test",
});
itemRegistry.register(item);
