# 🌈 Elemental Mod Loader

The Elemental Mod Loader for Minecraft. Requires the Elemental Mod Loader to be installed.

## Usage

### Creating a Mod

```typescript
import { Mod, IItem } from "./index";

const myMod = new Mod({
  id: "mymod",
});
```

### Creating a Simple Item

```typescript
const itemRegistry = myMod.createRegistry<IItem>("item");

const item = myMod.createItem({
  id: "test",
});
itemRegistry.register(item);
```

### Launching

Once you have created your mod in Javascript, you can drop the `.mjs` file into
the scripts folder and it will be picked up automatically when you launch the game.

### More Information

[See the gitlab repository for more information](https://gitlab.com/creepinson/elemental-loader).
