import { build } from "esbuild";

build({
  bundle: true,
  minify: process.env.NODE_ENV === "production",
  entryPoints: ["src/index.ts", "src/test.ts"],
  format: "esm",
  outdir: "dist",
})
  .then(() => {
    console.info("Done.");
  })
  .catch((err) => {
    console.error(err);
    process.exit(1);
  });
