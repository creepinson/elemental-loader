import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    kotlin("jvm") version "1.5.31"
    java
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "com.theoparis"
version = "1.0.0"

repositories {
    mavenCentral()
    maven("https://jitpack.io")
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib:1.5.21")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.8.1")
    implementation("net.bytebuddy:byte-buddy:1.11.20")
    implementation("net.bytebuddy:byte-buddy-agent:1.11.20")
    implementation("com.google.code.gson:gson:2.8.8")
// https://mvnrepository.com/artifact/org.graalvm.sdk/graal-sdk
    implementation("org.graalvm.sdk:graal-sdk:21.2.0")
    // https://mvnrepository.com/artifact/org.graalvm.truffle/truffle-api
    implementation("org.graalvm.truffle:truffle-api:21.2.0")
// https://mvnrepository.com/artifact/org.graalvm.js/js-scriptengine
    implementation("org.graalvm.js:js-scriptengine:21.2.0")
    implementation("org.graalvm.js:js:21.2.0")
    implementation("org.graalvm.compiler:compiler:21.2.0")
    implementation("org.graalvm.compiler:compiler-management:21.2.0")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}

tasks.register<JavaExec>("genMappings") {
    mainClass.set("com.theoparis.elementalbridge.mappings.MappingsGenerator")
    classpath = sourceSets["main"].runtimeClasspath
    args = mutableListOf("src/main/resources/mappings.json")
}

tasks {
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "11"
    }

    named<ShadowJar>("shadowJar") {
        mergeServiceFiles()
        manifest {
            attributes(
                mapOf(
                    "Premain-Class" to "com.theoparis.elementalbridge.ElementalBridgeEntry",
                    "Can-Retransform-Classes" to "true"
                )
            )
        }
        archiveClassifier.set("")
    }

    build {
        dependsOn(shadowJar)
    }
}
